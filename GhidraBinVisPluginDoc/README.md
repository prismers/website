# Ghidra Binary Visualization Plugin User Guide
<!-- ### Install with GUI -->
## Prerequisite

- Ghidra with GUI &gt;= v10.3.2
- The script

Open <code>Code Broswer</code>. (the classic Ghidra dragon head)\
<img src="images/codeBrowser.png" width="40" height="50">

Select <code>Script Manager</code> on the tool bar\
<img src="images/ScriptManager.png" width="40" height="40">

Select <code>create new script</code>, Select <code>Python</code>, name the script, and you will see a code space pop up\
<img src="images/createNewScript.png" width="40" height="40">\
<img src="images/codeSpace.png" width="300">

Then Simply place the entire <code>Script</code> in, then exit

Tick the box on the left\
<img src="images/Tick.png" width="300">

You will see load json On Tools->Misc->loadjson\
<img src="images/loadJson.png" width="300">

## Usage
To use the script, prepare your report in json and the analyzed binary.

Click load json, select the json

You will see the details of each bug in <code>Bookmark</code> of type <code>Note</code>
<img src="images/bookmark.png" width="700" height="90">

On the decompiler window, you can see the trace: Now, Previous, Next, click on the <code>address</code> to navigate the trace
<img src="images/decompiler.png" width="700">