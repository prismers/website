#The script will highlight all the address provided in a json for better debugging experience
#@author 
#@category _NEW_
#@keybinding 
#@menupath Tools.Misc.load json
#@toolbar 


#TODO Add User Code Here
import json
from ghidra.program.model.listing.CodeUnit import *
from ghidra.program.model.listing.Listing import *


#---------------------------------------
#|Step 2 / 7                           |
#---------------------------------------
#Now : Return <b>null</b> to caller
#
#Pre : <40916b>: ...
#Next: <40b334>: ...
#
#---------------------------------------



def getAddress(offset):
	return currentProgram.getAddressFactory().getDefaultAddressSpace().getAddress(offset)

# pop up a window, ask to select the file directory, open it
file = askFile('give me the json', 'there you go')
f = open(file.toString())

# load json from a file
data = json.load(f)

# get the list of bugTypes, e.g. 2 bugs in the example file
bugTypes = data['BugTypes']


addrSet = createAddressSet()
listing = currentProgram.getListing()


for bugType in bugTypes:
	for j,bug in enumerate(bugType['Reports']):
		theWholeTrace = []
		firstAddr = ''
		totalsteps = len(bug['DiagSteps'])
		for step in bug['DiagSteps']:
			
			theWholeTrace.append('<' + step['Addr'] + '>' + ': ' + step['Tip'])

		for index,step in enumerate(bug['DiagSteps']):
			# filter the 'Addr:0' case
			if int(step['Addr'],16) == 0:
				print('invalid addr: ' + addr.toString())
				continue
	
			addr = getAddress(int(step['Addr'],16))

			# store the first addr for bookmarking
			if firstAddr == '':
				firstAddr = addr

			codeUnit = listing.getCodeUnitAt(addr)

			# set comment
			codeUnit.setCommentAsArray(codeUnit.PLATE_COMMENT, theWholeTrace[:index])

			
			template = '---------------------------------------\n'
			template += '|Step %d / %d                           |\n' % (index+1, totalsteps)
			template += '---------------------------------------\nNow : %s\n' % (step['Tip'])
			if index-1 >= 0:
				template += '\nPre : <%s>: %s' % (bug['DiagSteps'][index-1]['Addr'], bug['DiagSteps'][index-1]['Tip'])

			else:
				template += '\nPre : N/A'

			if index+1 <= totalsteps - 1:
				template += '\nNext: <%s>: %s\n\n---------------------------------------\n' %(bug['DiagSteps'][index+1]['Addr'],bug['DiagSteps'][index-1]['Tip'])

			else:
				template += '\nNext: N/A\n\n---------------------------------------\n'

			codeUnit.setCommentAsArray(codeUnit.PRE_COMMENT, [template])
			addrSet.add(addr)

		# generate bookmark
		createBookmark(firstAddr,bugType['Name']+' / '+bugType['Description']+' <'+'%02d'%(j+1,)+'>','total no. of steps: <'+str(index+1)+'>' )

createHighlight(addrSet)

	
