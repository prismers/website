
# Clearblue wiki

![Clearblue system overview​](./images/Clearblue System Overview.png)

**Clearblue** is a sparse program analysis on top of lifted binary programs​, aiming to address the CODA challenges​:

- (**C**ontinuous) Incremental analysis with CI/CD pipeline-based integration​

- (**O**pen) Off-the-shelf APIs and SDKs​

- (**D**ark) Lifted dark code for precise analysis​

- (**A**ssembled) Fast function pointer analysis resolving deep callbacks​

**If you would like to have a try on *Clearblue*, please click [here](https://code.clearblueinnovations.org/?folder=/home/coder/workspace) to enjoy the *WebIDE Demo*!**

# Clearblue WebIDE User Guide

This is a user guide for install **Clearblue WebIDE v1.0.0** on top of Visual Studio Code. 

## Installation
----

### Install with Docker

Installing Clearblue WebIDE with docker is recommended as long as installing a [docker](https://docs.docker.com/engine/install/) or [docker-rootless](https://docs.docker.com/engine/security/rootless/) is not hard for you. The given image integrates Clearblue WebIDE with [Code Server](https://github.com/coder/code-server), making it easy for remote development.

**Prerequisite**

- Docker >= 23.0.0

**Install**

Log in to gitlab. You can use gitlab username & password. Or try [Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens) !
```bash
docker login
```
Pull the latest image from container registry.
```bash
docker pull prismers/cbvis:latest
```
Start Code Server and listening to 0.0.0.0:8080 on host machine.
```bash
docker run -d --name cbvis -p 8080:8080 prismers/cbvis:latest
```
Open your browser and visit http://127.0.0.1:8080. And now, you are free to have a try with Clearblue WebIDE.

**Update**

To update the image
```bash
docker pull registry.gitlab.com/prismers/cbvis:latest
```
But all changes made to original container are lost.  

To update artifacts in container (extension, cbcheck, plankton, etc)
```bash
update.sh
```

### Install with VSCode

**Prerequisite**

- VS Code >= v1.68.0
- Clearblue >= v1.0.0
- (Recommended) ![C/C++ for VS Code Extension]

Please first download the latest extension (end with `.vsix`) from artifacts in [CI pipeline](https://gitlab.com/prismers/cbvis/-/pipelines).

Open VS Code, go to extensions view (Ctrl+Shift+X), click `...` from the top right panel, select "install from vsix", then choose the extension file `cbvis-1.0.0.vsix`.

![install Clearblue WebIDE](/images/install%20Clearblue WebIDE.png)

Once a little bug icon appears, the installation is successful. Click the icon to enter the main view.

## Usage

This part introduces the basic usage of Clearblue WebIDE.

### WebIDE Initialization

Docker users can skip this section as everything is set already.

> **Tips for docker users**  
Code Server supports uploading files in drag-and-drop manner. Or you could also use [`docker cp`](https://docs.docker.com/engine/reference/commandline/cp/). But be aware that file permissions may not be preserved, which may cause malfunction with Clearblue WebIDE's scan-binary feature. We strongly recommend uploading file as .tar.

Users should first add the local Clearblue's cbcheck path in the extension settings. User can get into setting through welcome page, or

User Setting (Ctrl+,) => Search *Clearblue WebIDE.Clearblue WebIDE.cbcheckPath* => Input cbcheck path

<!-- ![cbcheck setting](/images/clearblue-setting-input.png) -->

### Project Management

Users can freely add, delete, and update items. Project information will be recorded in the workspace, so that the project still exists after restarting vscode.

#### Add New Projects

Click **New Project** button on welcome page or click **Add** icon beside projects manage view. The **New Project Page** will display as below.

<!-- ![new project](/images/new-project.png) -->

There are a few input fields:

 - Project Path - Folder. Path of this project.
 - Checker - Select a built-in checker
 - Number of Workers - workers to use for CbCheck
 - Custom Checker - Load custom checkers
 - Append Options - Cbcheck option. Default: -ps-stable (Optional)

**Project Path** is a required field to create a project. Click the icon on the right of input field to select the corresponding file or folder.

#### Delete Projects

After creating a project, the project will be displayed in the left view. Click the **delete icon** to delete the project.

<!-- ![delete project](/images/delete-project.png) -->

#### Save Projects

Click project item in the view of Project Manage to enter the project manage page. The user can modify the project information as in [New Project](#add-new-projects). Click the **Save** button and the system will save the information.

<!-- ![save project](/images/update-project.png) -->

### Run

You can use Clearblue with Clearblue WebIDE in two steps. First you need to run extract to get bitcode files. After bitcode gets extracted, you can check bitcode files for bugs.

> **Tips for large programs**  
Checking large program may make hours. To save your time, you can close Clearblue WebIDE or VSCode and do whatever you want after you see "project updated successfully" following any button press. When you come back, the status and progress will be restored.

#### Analysis

You can modify the cbcheck path in [user setting](#cbcheck-path-setting). After clicking the **Extract** button, the table below will indicate status and progress of extraction. After extraction, you can then click **CbCheck** button and wait for results.

<!-- ![run cbcheck](/images/run-cbcheck.png) -->

#### Diagnostics

Bugs are displayed as diagnostics in the corresponding line of the corresponding file. Support jumping in the floating window of diagnosis and PROBLEMS.

<!-- ![diagnostic](/images/diagnostic.png) -->

#### Bugs Treeview

After analyzing the report, a treeview will appear in the Bugs Report on the left. The first layer is the Bugs type, the second layer is the Bugs location, and the third layer is the Bugs step. Click the third layer to jump to the corresponding line of the corresponding file.

<!-- ![bugs treeiew](/images/bugs-overview.png) -->

#### Defect Overview

Click on the project item to open the analysis view, click DEFECT to display the Bugs overview. Some function usage tips:
- Click on the table header to sort.
- Clicking on a specific POSITION will jump to the corresponding error location, and the bugs tree view will expand accordingly.

<!-- ![bugs overview](/images/bugs-overview.png) -->

#### Report Charts

Click on a project item to open the analysis view, and clicking on CHARTS displays a summary chart.

<!-- ![analysis charts](/images/analysis-charts.png) -->

## Report Bugs

Clearblue WebIDE stores temporary files such as BCs, original bug reports, and logs. However, extracting and checking these files may sometimes cause crashes due to incompatible binaries, memory issues, or segmentation faults. In case of any errors, it's recommended to refer to the log files for more information about the issue. When reporting a problem, please remember to attach the relevant log files for better understanding of the situation.

## Checker Development

TODO